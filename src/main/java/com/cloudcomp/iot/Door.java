package com.cloudcomp.iot;

public class Door {
    private Integer id;
    private String description;
    private Boolean isOpened;

    public Door(Integer id, String description, Boolean isOpened) {
        this.id = id;
        this.description = description;
        this.isOpened = isOpened;
    }

    public Integer getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getOpened() {
        return isOpened;
    }

    public void setOpened(Boolean opened) {
        isOpened = opened;
    }

    public Door changeState() {
        this.isOpened = !isOpened;
        return this;
    }
}
