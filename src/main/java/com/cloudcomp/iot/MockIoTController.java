package com.cloudcomp.iot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/cc")
public class MockIoTController {
    private MockDB mockDB;

    @Autowired
    public MockIoTController(MockDB mockDB) {
        this.mockDB = mockDB;
    }

    @GetMapping(value = "/doors")
    public List<Door> getAllDoors() {
        return mockDB.getDoors();
    }

    @PostMapping(value = "/doors/{id}")
    public Door changeDoorState(@PathVariable("id") Integer doorId) {
        final Door door = mockDB.findDoorById(doorId).changeState();
        return door;
    }
}
