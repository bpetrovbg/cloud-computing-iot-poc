package com.cloudcomp.iot;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Component
public class MockDB {
    private List<Door> doors;

    public MockDB() {
        doors = new ArrayList<>();

        doors.add(new Door(1, "Garage", false));
        doors.add(new Door(2, "Home", false));
    }

    public List<Door> getDoors() {
        return doors;
    }

    public Door findDoorById(Integer doorId) {
        return doors.stream().filter(d -> d.getId() == doorId).findFirst().get();
    }
}
